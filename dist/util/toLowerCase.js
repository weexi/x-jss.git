/**
 * @description uppercase to lowercase
 * @param{string} v
 * @returns {string}
 */
module.exports = function (v) {
  var reg = /[A-Z]/g;
  if (!!v.match(reg)) {
    var match = v.match(reg).join('').toLowerCase().split('');
    var words = v.split(reg);
    var result  = [];
    for (var i = 0; i < words.length; i++) {
      if (i !== 0) {
        result.push(match[i - 1] + words[i])
      } else {
        result.push(words[i])
      }
    }
    return result.join('-')
  } else {
    return v
  }
}