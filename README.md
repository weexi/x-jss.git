# JStylesheet

#### 介绍
【原创】利用JavaScript写CSS吧！


 **使用** 

```javascript
var JSS = require("jstylesheet");
var style = new JSS({
    body: {
        margin: 0,
        padding: 0,
        div: {
            fontSize: "10px",
            "&.top-div": {
                color: 'red'
            },
            "&.bottom-div": {
                color: "blue"
            },
            "&:first-child": {
                fontSize: "12px"
            },
            "&:last-child": {
                fontSize: "14px"
            },
            "&:not(:first-child)":{
                textAlgin: "right"
            },
            $hover: {
                boxShadow: "0 0 10px #eee"
            },
            span: {
                color: 'yellow'
            }
        }
    }
}).compile();
```


```javascript
// 自动添加到<Head>元素内
style.append()
```

 **在浏览器中使用** 
```html
<script src="jss.browser.js"></script>
<script>
var style = new JStyleSheet({
    ...
}).compile();
style.append()
</script>
```

// 生成CSS ==>


```html
body { margin: 0; padding: 0 }
body div { font-size: 10px }
body div.top-div { color: 'red' }
body div.bottom-div { color: 'blue' }
body div:first-child { font-size: '12px' }
body div:last-child { font-size: '14px' }
body div:not(:first-child) { text-align: 'right' }
body div:hover { boxShadow: "0 0 10px #eee" }
body div span { color: 'yellow' }
```


